<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Throwable;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $e)
    {
        parent::report($e);
    }

    public function render($request, Throwable $e)
    {
        $response = $this->handleException($request, $e);
        return $response;
    }

    public function register()
    {
        $this->renderable(function (Exception $exception) {
        });
    }

    public function handleException($request, Exception $exception)
    {
        //VALIDAR EXEPCIONES EN EL INGRESO DE DATOS DEL REQUEST
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        //VALIDAR EXCEPCIONES AL NO CONSEGUIR DATOS EN LAS BUSQUEDAS
        if ($exception instanceof ModelNotFoundException) {
            $modelo = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("No existe ningun registro para mostrar con el ID especificado", 404);
        }

        //VALIDAR EXCEPCIONES PARA PERMISOS A LAS ACCIONES DEL API
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse("No tiene permisos para ejecutar esta accion", 403);
        }

        //VALIDAR EXCEPCIONES PARA LAS URL INCORRECTAS
        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse("No se encontro la URL especificada", 404);
        }

        //VALIDAR EXCEPCION PARA METODOS NO VALIDOS
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse("El método espécificado en la peticion no es válido", 405);
        }

        //VALIDAR EXCEPCION GENERAL PARA CUALQUIER PETICIONES HTTP
        if ($exception instanceof HttpException) {
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        //VALIDAR EXCEPCION PARA ERRORES AL ELIMINAR ELEMENTOS QUE TENGAN CLAVE FORANEA
        if ($exception instanceof QueryException) {
            //dd($exception);

            //Obtener el codigo del error de la excepcion
            $codigo = $exception->errorInfo[1];

            if ($codigo == 1451) {
                return $this->errorResponse("No se puede eliminar de forma permanente el recurso porque esta relacionado con algún otro.", 409);
            }
        }

        //VALIDACION PARA LOS ERRORES DE CSRF TOKEN
        if ($exception instanceof TokenMismatchException) {
            return redirect()->back()->withInput($request->input());
        }

        //VALIDAR EN QUE ENTORNO DE DESARROLLO ESTAMOS TRABAJANDO, SI ES EN DESARROLLO MOSTRARA
        //EL ERROR COMPLETO, SINO MOSTRARA UN ERROR 500
        if (config('app.debug')) {
            return parent::render($request, $exception);
        } else {
            //VALIDAR EXCEPCION PARA CUALQUIER FALLA INESPERADA
            return $this->errorResponse("Error interno del servidor", 500);
        }
    }


    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();

        return $this->errorResponse($errors, 422);
    }
}
