<?php

namespace App\Models;

use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'status',
    ];

    public $transformer = CategoryTransformer::class;

    public function setNameAttribute($valor)
    {
        $this->attributes['name'] = strtoupper($valor);
    }

    public function pilots()
    {
        return $this->hasMany(Pilot::class);
    }

    public function races()
    {
        return $this->hasMany(Race::class);
    }

    public function helps()
    {
        return $this->belongsToMany(Help::class, 'helps_has_categories', 'help_id', 'category_id')->withTimestamps();
    }
}
