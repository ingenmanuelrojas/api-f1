<?php

namespace App\Models;

use App\Transformers\HelpTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    public $transformer = HelpTransformer::class;

    public function setNameAttribute($valor)
    {
        $this->attributes['name'] = strtoupper($valor);
    }

    public function setDescriptionAttribute($valor)
    {
        $this->attributes['description'] = strtoupper($valor);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'helps_has_categories', 'category_id', 'help_id')->withTimestamps();
    }
}
