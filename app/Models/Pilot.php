<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pilot extends Model
{
    use HasFactory;

    protected $fillable = [
        'nickname',
        'first_name',
        'last_name',
        'birthdate',
        'phone',
        'email',
        'picture',
        'country_id',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function helps()
    {
        return $this->belongsToMany(Help::class, 'pilot_has_helps', 'help_id', 'pilot_id')->withTimestamps();
    }

    public function races()
    {
        return $this->belongsToMany(Race::class, 'pilot_has_races', 'race_id', 'pilot_id')->withTimestamps();
    }
}
