<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'hour',
        'country_id',
        'race_id',
    ];

    public function contry()
    {
        return $this->belongsTo(Country::class);
    }

    public function race()
    {
        return $this->belongsTo(Race::class);
    }
}
