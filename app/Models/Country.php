<?php

namespace App\Models;

use App\Transformers\CountryTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    public $transformer = CountryTransformer::class;

    protected $fillable = [
        'code',
        'name',
        'flag',
    ];

    public function pilots()
    {
        return $this->hasMany(Pilot::class);
    }

    public function races()
    {
        return $this->hasMany(Race::class);
    }

    public function shedules()
    {
        return $this->hasMany(Shedule::class);
    }
}
