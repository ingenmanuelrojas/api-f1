<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penalty extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'time',
        'description'
    ];

    public function races()
    {
        return $this->belongsToMany(Race::class, 'race_has_penalties', 'race_id', 'penalty_id')->withTimestamps();
    }
}
