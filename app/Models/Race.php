<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'date',
        'country_id',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function contry()
    {
        return $this->belongsTo(Country::class);
    }

    public function shedules()
    {
        return $this->hasMany(Shedule::class);
    }

    public function pilots()
    {
        return $this->belongsToMany(Pilot::class, 'pilot_has_races', 'pilot_id', 'race_id')->withTimestamps();
    }

    public function penalties()
    {
        return $this->belongsToMany(Penalty::class, 'race_has_penalties', 'penalty_id', 'race_id')->withTimestamps();
    }
}
