<?php

namespace App\Transformers;

use App\Models\Country;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Str;


class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Country $country)
    {
        return [
            'id'                  => (int)$country->id,
            'codigo'              => (string)$country->code,
            'nombre'              => (string)$country->name,
            'bandera'             => (string)env("APP_URL") . '/uploads/flags/' . Str::lower($country->code) . '.png',
            'fecha_creacion'      => (string)$country->created_at,
            'fecha_actualizacion' => (string)$country->updated_at,
            'fecha_eliminacion'   => isset($country->deleted_at) ? (string) $country->deleted_at : null,
        ];
    }
}
