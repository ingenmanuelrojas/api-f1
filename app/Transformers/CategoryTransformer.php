<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;
use phpDocumentor\Reflection\Types\Boolean;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id'                  => (int)$category->id,
            'nombre'              => (string)$category->name,
            'estatus'             => (bool)$category->status,
            'fecha_creacion'      => (string)$category->created_at,
            'fecha_actualizacion' => (string)$category->updated_at,
            'fecha_eliminacion'   => isset($category->deleted_at) ? (string) $category->deleted_at : null,
        ];
    }
}
