<?php

namespace App\Transformers;

use App\Models\Help;
use League\Fractal\TransformerAbstract;

class HelpTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Help $help)
    {
        return [
            'id'                  => (int)$help->id,
            'nombre'              => (string)$help->name,
            'descripcion'         => (string)$help->description,
            'estatus'             => (bool)$help->status,
            'fecha_creacion'      => (string)$help->created_at,
            'fecha_actualizacion' => (string)$help->updated_at,
            'fecha_eliminacion'   => isset($help->deleted_at) ? (string) $help->deleted_at : null,
        ];
    }
}
