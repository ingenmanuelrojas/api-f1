<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Log;

trait ApiResponser
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json([
            "status" => "error",
            "error"  => $message,
            "code"   => $code,
        ], $code);
    }

    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }
        $transformer = $collection->first()->transformer; //OBTENER EL PRIMER ELEMENTO DE LA COLECCION
        $collection = $this->transformData($collection, $transformer);

        return $this->successResponse([
            "status"   => "success",
            "code"     => $code,
            "response" => $collection
        ], $code);
    }

    protected function showOne(Model $instance, $message = "", $code = 200)
    {
        $transformer = $instance->transformer;
        $data = $this->transformData($instance, $transformer);

        return $this->successResponse([
            "status"   => "success",
            "code"     => $code,
            "message"  => $message,
            "response" => $data
        ], $code);
    }

    protected function transformData($data, $transformer)
    {
        $transformation = fractal($data, new $transformer);

        return $transformation->toArray();
    }
}
