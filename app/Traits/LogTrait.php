<?php


namespace App\Traits;

use Illuminate\Support\Facades\Log;

trait LogTrait
{
    public function controlarExcepcion($excepcion, $previusException = null){
        if ($previusException) {
            $trazaPrevia = $previusException->getTrace()[0];
            $dataPrevia = array(
                'message' => $previusException->getMessage(),
                'file'   => $trazaPrevia['file'],
                'class' => $trazaPrevia['class'],
                'method' => $trazaPrevia['function'],
                'line' => $trazaPrevia['line'],
                'code' => $previusException->getCode()
            );
            Log::error("ErrorOriginal", $dataPrevia);
        }


        $traza = $excepcion->getTrace()[0];
        $data = array(
            'message' => $excepcion->getMessage(),
            'file'   => $traza['file'],
            'class' => $traza['class'],
            'method' => $traza['function'],
            'line' => $traza['line'],
            'code' => $excepcion->getCode()
        );

        Log::error("Error", $data);
    }
}
