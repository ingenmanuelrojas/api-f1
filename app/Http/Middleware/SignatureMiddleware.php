<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SignatureMiddleware
{
    public function handle($request, Closure $next, $headers = 'X-Name')
    {
        $response = $next($request);

        $response->headers->set($headers, config('app.name'));

        return $response;
    }
}
