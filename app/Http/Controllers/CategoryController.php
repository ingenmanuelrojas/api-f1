<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Log;
use Throwable;

class CategoryController extends ApiController
{

    public function index()
    {
        $categories = Category::all();

        return $this->showAll($categories);
    }


    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->all());
        return $this->showOne($category, "Categoria creada con exito.", 201);
    }


    public function show(Category $category)
    {
        return $this->showOne($category);
    }


    public function update(CategoryRequest $request, Category $category)
    {
        $category->name   = $request->name;
        $category->status = $request->status;
        $category->save();

        return $this->showOne($category, "Categoria actualizada con exito.");
    }


    public function destroy(Category $category)
    {
        $category->delete();
        return $this->showOne($category, "Categoria eliminada con exito.");
    }
}
