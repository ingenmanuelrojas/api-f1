<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Log;

class CountryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();

        return $this->showAll($countries);
    }


    public function show(Country $country)
    {
        return $this->showOne($country);
    }

    public function getCountryByCode($slug)
    {
        $country = Country::where('code', $slug)->first();
        Log::info($country);
        return $this->showOne($country);
    }
}
