<?php

namespace App\Http\Controllers;

use App\Http\Requests\HelpRequest;
use App\Models\Help;
use Illuminate\Http\Request;
use Log;

class HelpController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $helps = Help::all();
        Log::info($helps);

        return $this->showAll($helps);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HelpRequest $request)
    {
        $help = Help::create($request->all());
        Log::info($help);
        return $this->showOne($help, "Ayuda creada con exito.", 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Help $help)
    {
        return $this->showOne($help);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HelpRequest $request, Help $help)
    {
        $help->description = $request->description;
        $help->status      = $request->status;
        $help->save();

        return $this->showOne($help, "Ayuda actualizada con exito.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Help $help)
    {
        $help->delete();
        return $this->showOne($help, "Ayuda eliminada con exito.");
    }
}
