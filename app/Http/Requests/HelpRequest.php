<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HelpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                $rules = [
                    'name'        => ['required', 'unique:helps,name'],
                    'description' => ['required'],
                    'status'      => ['required', 'boolean'],
                ];
                break;
            case 'PUT':
                $rules = [
                    'name'        => ['required', Rule::unique('helps')->ignore($this->route('help'))],
                    'description' => ['required'],
                    'status'      => ['required', 'boolean'],
                ];
                break;
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'        => "El nombre es obligatorio",
            'nombre.unique'        => "El nombre ya esta registrado",
            'description.required' => "La descripción es obligatoria",
            'status.required'      => "El estatus es obligatorio",
            'status.boolean'       => "El estatus debe ser un boolean",
        ];
    }
}
