<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Log;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                $rules = [
                    'name'   => ['required', 'unique:categories,name'],
                    'status' => ['required', 'boolean'],
                ];
                break;
            case 'PUT':
                $rules = [
                    'name' => ['required', Rule::unique('categories')->ignore($this->route('category'))],
                    'status' => ['required', 'boolean'],
                ];
                break;
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'   => "El nombre es obligatorio",
            'name.unique'     => "El nombre de la categoria ya esta registrado",
            'status.required' => "El estatus es obligatorio",
            'status.boolean'  => "El estatus debe ser un boolean",
        ];
    }
}
