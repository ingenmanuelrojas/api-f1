<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\PenaltyController;
use App\Http\Controllers\PilotController;
use App\Http\Controllers\QualyController;
use App\Http\Controllers\RaceController;
use App\Http\Controllers\SheduleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**Categories*/
Route::apiResource('categories', CategoryController::class);

/**Helps */
Route::apiResource('helps', HelpController::class);

/**Countries */
Route::get('/countries/getCountryByCode/{code}', [CountryController::class, 'getCountryByCode']);
Route::apiResource('countries', CountryController::class)->except(['store', 'update', 'destroy']);

/**Pilots */
Route::resource('pilots', PilotController::class);
Route::resource('races', RaceController::class);
Route::resource('qualies', QualyController::class);
Route::resource('shedules', SheduleController::class);
Route::resource('penalties', PenaltyController::class);
