<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRaceHasPenaltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('race_has_penalties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pilot_id')->constrained('pilots');
            $table->foreignId('penalty_id')->constrained('penalties');
            $table->foreignId('race_id')->constrained('races');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('race_has_penalties');
    }
}
