<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePilotHasRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilot_has_races', function (Blueprint $table) {
            $table->id();
            $table->integer('position');
            $table->time('quick_turn')->nullable();
            $table->boolean('penalty')->default(0);
            $table->foreignId('pilot_id')->constrained('pilots');
            $table->foreignId('race_id')->constrained('races');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pilot_has_races');
    }
}
