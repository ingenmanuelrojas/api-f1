<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Country;
use App\Models\Pilot;
use Illuminate\Database\Eloquent\Factories\Factory;

class PilotFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pilot::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nickname'    => $this->faker->word(),
            'first_name'  => $this->faker->firstName(),
            'last_name'   => $this->faker->lastName(),
            'birthdate'   => $this->faker->date(),
            'phone'       => $this->faker->phoneNumber(),
            'email'       => $this->faker->email(),
            'picture'     => $this->faker->imageUrl(),
            'country_id'  => Country::all()->random()->id,
            'category_id' => Category::all()->random()->id,
        ];
    }
}
