<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HelpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('helps')->insert(
            [
                'name'        => 'ABS',
                'description' => 'Activo',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'ABS',
                'description' => 'Desactivado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Control de tracción',
                'description' => 'Completo',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Control de tracción',
                'description' => 'Medio',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Control de tracción',
                'description' => 'Desactivado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Daños monoplaza',
                'description' => 'Simulación',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Daños monoplaza',
                'description' => 'Completo',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Daños monoplaza',
                'description' => 'Reducido',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name' => 'Severidad saltarse curvas',
                'description' => 'Normal',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Severidad saltarse curvas',
                'description' => 'Estricto',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Normativa del parc fermé',
                'description' => 'Activado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Normativa del parc fermé',
                'description' => 'Desactivado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Asistencia de frenada',
                'description' => 'Alto',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Asistencia de frenada',
                'description' => 'Medio',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Asistencia de frenada',
                'description' => 'Bajo',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Asistencia de frenada',
                'description' => 'Desactivado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Frenos antibloqueo',
                'description' => 'Activado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Frenos antibloqueo',
                'description' => 'Desactivado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Trazada dinámica',
                'description' => 'Solo en las curvas',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Trazada dinámica',
                'description' => 'Todo el circuito',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Caja de cambios',
                'description' => 'Manual',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Caja de cambios',
                'description' => 'Automática',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Asistencia en boxes',
                'description' => 'Activado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );

        DB::table('helps')->insert(
            [
                'name'        => 'Asistencia en boxes',
                'description' => 'Desactivado',
                'status'      => 1,
                'created_at'  => now(),
                'updated_at'  => now(),
            ]
        );
    }
}
