<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'code' => 'AF',
            'name' => 'Afganistán',
        ]);

        DB::table('countries')->insert([
            'code' => 'AX',
            'name' => 'Islas Gland',
        ]);

        DB::table('countries')->insert([
            'code' => 'AL',
            'name' => 'Albania',
        ]);

        DB::table('countries')->insert([
            'code' => 'DE',
            'name' => 'Alemania',
        ]);

        DB::table('countries')->insert([
            'code' => 'AD',
            'name' => 'Andorra',
        ]);

        DB::table('countries')->insert([
            'code' => 'AO',
            'name' => 'Angola',
        ]);

        DB::table('countries')->insert([
            'code' => 'AI',
            'name' => 'Anguilla',
        ]);

        DB::table('countries')->insert([
            'code' => 'AQ',
            'name' => 'Antártida',
        ]);

        DB::table('countries')->insert([
            'code' => 'AG',
            'name' => 'Antigua y Barbuda',
        ]);

        DB::table('countries')->insert([
            'code' => 'AN',
            'name' => 'Antillas Holandesas',
        ]);

        DB::table('countries')->insert([
            'code' => 'SA',
            'name' => 'Arabia Saudí',
        ]);

        DB::table('countries')->insert([
            'code' => 'DZ',
            'name' => 'Argelia',
        ]);

        DB::table('countries')->insert([
            'code' => 'AR',
            'name' => 'Argentina',
        ]);

        DB::table('countries')->insert([
            'code' => 'AM',
            'name' => 'Armenia',
        ]);

        DB::table('countries')->insert([
            'code' => 'AW',
            'name' => 'Aruba',
        ]);

        DB::table('countries')->insert([
            'code' => 'AU',
            'name' => 'Australia',
        ]);

        DB::table('countries')->insert([
            'code' => 'AT',
            'name' => 'Austria',
        ]);

        DB::table('countries')->insert([
            'code' => 'AZ',
            'name' => 'Azerbaiyán',
        ]);

        DB::table('countries')->insert([
            'code' => 'BS',
            'name' => 'Bahamas',
        ]);

        DB::table('countries')->insert([
            'code' => 'BH',
            'name' => 'Bahréin',
        ]);

        DB::table('countries')->insert([
            'code' => 'BD',
            'name' => 'Bangladesh',
        ]);

        DB::table('countries')->insert([
            'code' => 'BB',
            'name' => 'Barbados',
        ]);

        DB::table('countries')->insert([
            'code' => 'BY',
            'name' => 'Bielorrusia',
        ]);

        DB::table('countries')->insert([
            'code' => 'BE',
            'name' => 'Bélgica',
        ]);

        DB::table('countries')->insert([
            'code' => 'BZ',
            'name' => 'Belice',
        ]);

        DB::table('countries')->insert([
            'code' => 'BJ',
            'name' => 'Benin',
        ]);

        DB::table('countries')->insert([
            'code' => 'BM',
            'name' => 'Bermudas',
        ]);

        DB::table('countries')->insert([
            'code' => 'BT',
            'name' => 'Bhután',
        ]);

        DB::table('countries')->insert([
            'code' => 'BO',
            'name' => 'Bolivia',
        ]);

        DB::table('countries')->insert([
            'code' => 'BA',
            'name' => 'Bosnia y Herzegovina',
        ]);

        DB::table('countries')->insert([
            'code' => 'BW',
            'name' => 'Botsuana',
        ]);

        DB::table('countries')->insert([
            'code' => 'BV',
            'name' => 'Isla Bouvet',
        ]);

        DB::table('countries')->insert([
            'code' => 'BR',
            'name' => 'Brasil',
        ]);

        DB::table('countries')->insert([
            'code' => 'BN',
            'name' => 'Brunéi',
        ]);

        DB::table('countries')->insert([
            'code' => 'BG',
            'name' => 'Bulgaria',
        ]);

        DB::table('countries')->insert([
            'code' => 'BF',
            'name' => 'Burkina Faso',
        ]);

        DB::table('countries')->insert([
            'code' => 'BI',
            'name' => 'Burundi',
        ]);

        DB::table('countries')->insert([
            'code' => 'CV',
            'name' => 'Cabo Verde',
        ]);

        DB::table('countries')->insert([
            'code' => 'KY',
            'name' => 'Islas Caimán',
        ]);

        DB::table('countries')->insert([
            'code' => 'KH',
            'name' => 'Camboya',
        ]);

        DB::table('countries')->insert([
            'code' => 'CM',
            'name' => 'Camerún',
        ]);

        DB::table('countries')->insert([
            'code' => 'CA',
            'name' => 'Canadá',
        ]);

        DB::table('countries')->insert([
            'code' => 'CF',
            'name' => 'República Centroafricana',
        ]);

        DB::table('countries')->insert([
            'code' => 'TD',
            'name' => 'Chad',
        ]);

        DB::table('countries')->insert([
            'code' => 'CZ',
            'name' => 'República Checa',
        ]);

        DB::table('countries')->insert([
            'code' => 'CL',
            'name' => 'Chile',
        ]);

        DB::table('countries')->insert([
            'code' => 'CN',
            'name' => 'China',
        ]);

        DB::table('countries')->insert([
            'code' => 'CY',
            'name' => 'Chipre',
        ]);

        DB::table('countries')->insert([
            'code' => 'CX',
            'name' => 'Isla de Navidad',
        ]);

        DB::table('countries')->insert([
            'code' => 'VA',
            'name' => 'Ciudad del Vaticano',
        ]);

        DB::table('countries')->insert([
            'code' => 'CC',
            'name' => 'Islas Cocos',
        ]);

        DB::table('countries')->insert([
            'code' => 'CO',
            'name' => 'Colombia',
        ]);

        DB::table('countries')->insert([
            'code' => 'KM',
            'name' => 'Comoras',
        ]);

        DB::table('countries')->insert([
            'code' => 'CD',
            'name' => 'República Democrática del Congo',
        ]);

        DB::table('countries')->insert([
            'code' => 'CG',
            'name' => 'Congo',
        ]);

        DB::table('countries')->insert([
            'code' => 'CK',
            'name' => 'Islas Cook',
        ]);

        DB::table('countries')->insert([
            'code' => 'KP',
            'name' => 'Corea del Norte',
        ]);

        DB::table('countries')->insert([
            'code' => 'KR',
            'name' => 'Corea del Sur',
        ]);

        DB::table('countries')->insert([
            'code' => 'CI',
            'name' => 'Costa de Marfil',
        ]);

        DB::table('countries')->insert([
            'code' => 'CR',
            'name' => 'Costa Rica',
        ]);

        DB::table('countries')->insert([
            'code' => 'HR',
            'name' => 'Croacia',
        ]);

        DB::table('countries')->insert([
            'code' => 'CU',
            'name' => 'Cuba',
        ]);

        DB::table('countries')->insert([
            'code' => 'DK',
            'name' => 'Dinamarca',
        ]);

        DB::table('countries')->insert([
            'code' => 'DM',
            'name' => 'Dominica',
        ]);

        DB::table('countries')->insert([
            'code' => 'DO',
            'name' => 'República Dominicana',
        ]);

        DB::table('countries')->insert([
            'code' => 'EC',
            'name' => 'Ecuador',
        ]);

        DB::table('countries')->insert([
            'code' => 'EG',
            'name' => 'Egipto',
        ]);

        DB::table('countries')->insert([
            'code' => 'SV',
            'name' => 'El Salvador',
        ]);

        DB::table('countries')->insert([
            'code' => 'AE',
            'name' => 'Emiratos Árabes Unidos',
        ]);

        DB::table('countries')->insert([
            'code' => 'ER',
            'name' => 'Eritrea',
        ]);

        DB::table('countries')->insert([
            'code' => 'SK',
            'name' => 'Eslovaquia',
        ]);

        DB::table('countries')->insert([
            'code' => 'SI',
            'name' => 'Eslovenia',
        ]);

        DB::table('countries')->insert([
            'code' => 'ES',
            'name' => 'España',
        ]);

        DB::table('countries')->insert([
            'code' => 'UM',
            'name' => 'Islas ultramarinas de Estados Unidos',
        ]);

        DB::table('countries')->insert([
            'code' => 'US',
            'name' => 'Estados Unidos',
        ]);

        DB::table('countries')->insert([
            'code' => 'EE',
            'name' => 'Estonia',
        ]);

        DB::table('countries')->insert([
            'code' => 'ET',
            'name' => 'Etiopía',
        ]);

        DB::table('countries')->insert([
            'code' => 'FO',
            'name' => 'Islas Feroe',
        ]);

        DB::table('countries')->insert([
            'code' => 'PH',
            'name' => 'Filipinas',
        ]);

        DB::table('countries')->insert([
            'code' => 'FI',
            'name' => 'Finlandia',
        ]);

        DB::table('countries')->insert([
            'code' => 'FJ',
            'name' => 'Fiyi',
        ]);

        DB::table('countries')->insert([
            'code' => 'FR',
            'name' => 'Francia',
        ]);

        DB::table('countries')->insert([
            'code' => 'GA',
            'name' => 'Gabón',
        ]);

        DB::table('countries')->insert([
            'code' => 'GM',
            'name' => 'Gambia',
        ]);

        DB::table('countries')->insert([
            'code' => 'GE',
            'name' => 'Georgia',
        ]);

        DB::table('countries')->insert([
            'code' => 'GS',
            'name' => 'Islas Georgias del Sur y Sandwich del Sur',
        ]);

        DB::table('countries')->insert([
            'code' => 'GH',
            'name' => 'Ghana',
        ]);

        DB::table('countries')->insert([
            'code' => 'GI',
            'name' => 'Gibraltar',
        ]);

        DB::table('countries')->insert([
            'code' => 'GD',
            'name' => 'Granada',
        ]);

        DB::table('countries')->insert([
            'code' => 'GR',
            'name' => 'Grecia',
        ]);

        DB::table('countries')->insert([
            'code' => 'GL',
            'name' => 'Groenlandia',
        ]);

        DB::table('countries')->insert([
            'code' => 'GP',
            'name' => 'Guadalupe',
        ]);

        DB::table('countries')->insert([
            'code' => 'GU',
            'name' => 'Guam',
        ]);

        DB::table('countries')->insert([
            'code' => 'GT',
            'name' => 'Guatemala',
        ]);

        DB::table('countries')->insert([
            'code' => 'GF',
            'name' => 'Guayana Francesa',
        ]);

        DB::table('countries')->insert([
            'code' => 'GN',
            'name' => 'Guinea',
        ]);

        DB::table('countries')->insert([
            'code' => 'GQ',
            'name' => 'Guinea Ecuatorial',
        ]);

        DB::table('countries')->insert([
            'code' => 'GW',
            'name' => 'Guinea-Bissau',
        ]);

        DB::table('countries')->insert([
            'code' => 'GY',
            'name' => 'Guyana',
        ]);

        DB::table('countries')->insert([
            'code' => 'HT',
            'name' => 'Haití',
        ]);

        DB::table('countries')->insert([
            'code' => 'HM',
            'name' => 'Islas Heard y McDonald',
        ]);

        DB::table('countries')->insert([
            'code' => 'HN',
            'name' => 'Honduras',
        ]);

        DB::table('countries')->insert([
            'code' => 'HK',
            'name' => 'Hong Kong',
        ]);

        DB::table('countries')->insert([
            'code' => 'HU',
            'name' => 'Hungría',
        ]);

        DB::table('countries')->insert([
            'code' => 'IN',
            'name' => 'India',
        ]);

        DB::table('countries')->insert([
            'code' => 'ID',
            'name' => 'Indonesia',
        ]);

        DB::table('countries')->insert([
            'code' => 'IR',
            'name' => 'Irán',
        ]);

        DB::table('countries')->insert([
            'code' => 'IQ',
            'name' => 'Iraq',
        ]);

        DB::table('countries')->insert([
            'code' => 'IE',
            'name' => 'Irlanda',
        ]);

        DB::table('countries')->insert([
            'code' => 'IS',
            'name' => 'Islandia',
        ]);

        DB::table('countries')->insert([
            'code' => 'IL',
            'name' => 'Israel',
        ]);

        DB::table('countries')->insert([
            'code' => 'IT',
            'name' => 'Italia',
        ]);

        DB::table('countries')->insert([
            'code' => 'JM',
            'name' => 'Jamaica',
        ]);

        DB::table('countries')->insert([
            'code' => 'JP',
            'name' => 'Japón',
        ]);

        DB::table('countries')->insert([
            'code' => 'JO',
            'name' => 'Jordania',
        ]);

        DB::table('countries')->insert([
            'code' => 'KZ',
            'name' => 'Kazajstán',
        ]);

        DB::table('countries')->insert([
            'code' => 'KE',
            'name' => 'Kenia',
        ]);

        DB::table('countries')->insert([
            'code' => 'KG',
            'name' => 'Kirguistán',
        ]);

        DB::table('countries')->insert([
            'code' => 'KI',
            'name' => 'Kiribati',
        ]);

        DB::table('countries')->insert([
            'code' => 'KW',
            'name' => 'Kuwait',
        ]);

        DB::table('countries')->insert([
            'code' => 'LA',
            'name' => 'Laos',
        ]);

        DB::table('countries')->insert([
            'code' => 'LS',
            'name' => 'Lesotho',
        ]);

        DB::table('countries')->insert([
            'code' => 'LV',
            'name' => 'Letonia',
        ]);

        DB::table('countries')->insert([
            'code' => 'LB',
            'name' => 'Líbano',
        ]);

        DB::table('countries')->insert([
            'code' => 'LR',
            'name' => 'Liberia',
        ]);

        DB::table('countries')->insert([
            'code' => 'LY',
            'name' => 'Libia',
        ]);

        DB::table('countries')->insert([
            'code' => 'LI',
            'name' => 'Liechtenstein',
        ]);

        DB::table('countries')->insert([
            'code' => 'LT',
            'name' => 'Lituania',
        ]);

        DB::table('countries')->insert([
            'code' => 'LU',
            'name' => 'Luxemburgo',
        ]);

        DB::table('countries')->insert([
            'code' => 'MO',
            'name' => 'Macao',
        ]);

        DB::table('countries')->insert([
            'code' => 'MK',
            'name' => 'ARY Macedonia',
        ]);

        DB::table('countries')->insert([
            'code' => 'MG',
            'name' => 'Madagascar',
        ]);

        DB::table('countries')->insert([
            'code' => 'MY',
            'name' => 'Malasia',
        ]);

        DB::table('countries')->insert([
            'code' => 'MW',
            'name' => 'Malawi',
        ]);

        DB::table('countries')->insert([
            'code' => 'MV',
            'name' => 'Maldivas',
        ]);

        DB::table('countries')->insert([
            'code' => 'ML',
            'name' => 'Malí',
        ]);

        DB::table('countries')->insert([
            'code' => 'MT',
            'name' => 'Malta',
        ]);

        DB::table('countries')->insert([
            'code' => 'FK',
            'name' => 'Islas Malvinas',
        ]);

        DB::table('countries')->insert([
            'code' => 'MP',
            'name' => 'Islas Marianas del Norte',
        ]);

        DB::table('countries')->insert([
            'code' => 'MA',
            'name' => 'Marruecos',
        ]);

        DB::table('countries')->insert([
            'code' => 'MH',
            'name' => 'Islas Marshall',
        ]);

        DB::table('countries')->insert([
            'code' => 'MQ',
            'name' => 'Martinica',
        ]);

        DB::table('countries')->insert([
            'code' => 'MU',
            'name' => 'Mauricio',
        ]);

        DB::table('countries')->insert([
            'code' => 'MR',
            'name' => 'Mauritania',
        ]);

        DB::table('countries')->insert([
            'code' => 'YT',
            'name' => 'Mayotte',
        ]);

        DB::table('countries')->insert([
            'code' => 'MX',
            'name' => 'México',
        ]);

        DB::table('countries')->insert([
            'code' => 'FM',
            'name' => 'Micronesia',
        ]);

        DB::table('countries')->insert([
            'code' => 'MD',
            'name' => 'Moldavia',
        ]);

        DB::table('countries')->insert([
            'code' => 'MC',
            'name' => 'Mónaco',
        ]);

        DB::table('countries')->insert([
            'code' => 'MN',
            'name' => 'Mongolia',
        ]);

        DB::table('countries')->insert([
            'code' => 'MS',
            'name' => 'Montserrat',
        ]);

        DB::table('countries')->insert([
            'code' => 'MZ',
            'name' => 'Mozambique',
        ]);

        DB::table('countries')->insert([
            'code' => 'MM',
            'name' => 'Myanmar',
        ]);

        DB::table('countries')->insert([
            'code' => 'NA',
            'name' => 'Namibia',
        ]);

        DB::table('countries')->insert([
            'code' => 'NR',
            'name' => 'Nauru',
        ]);

        DB::table('countries')->insert([
            'code' => 'NP',
            'name' => 'Nepal',
        ]);

        DB::table('countries')->insert([
            'code' => 'NI',
            'name' => 'Nicaragua',
        ]);

        DB::table('countries')->insert([
            'code' => 'NE',
            'name' => 'Níger',
        ]);

        DB::table('countries')->insert([
            'code' => 'NG',
            'name' => 'Nigeria',
        ]);

        DB::table('countries')->insert([
            'code' => 'NU',
            'name' => 'Niue',
        ]);

        DB::table('countries')->insert([
            'code' => 'NF',
            'name' => 'Isla Norfolk',
        ]);

        DB::table('countries')->insert([
            'code' => 'NO',
            'name' => 'Noruega',
        ]);

        DB::table('countries')->insert([
            'code' => 'NC',
            'name' => 'Nueva Caledonia',
        ]);

        DB::table('countries')->insert([
            'code' => 'NZ',
            'name' => 'Nueva Zelanda',
        ]);

        DB::table('countries')->insert([
            'code' => 'OM',
            'name' => 'Omán',
        ]);

        DB::table('countries')->insert([
            'code' => 'NL',
            'name' => 'Países Bajos',
        ]);

        DB::table('countries')->insert([
            'code' => 'PK',
            'name' => 'Pakistán',
        ]);

        DB::table('countries')->insert([
            'code' => 'PW',
            'name' => 'Palau',
        ]);

        DB::table('countries')->insert([
            'code' => 'PS',
            'name' => 'Palestina',
        ]);

        DB::table('countries')->insert([
            'code' => 'PA',
            'name' => 'Panamá',
        ]);

        DB::table('countries')->insert([
            'code' => 'PG',
            'name' => 'Papúa Nueva Guinea',
        ]);

        DB::table('countries')->insert([
            'code' => 'PY',
            'name' => 'Paraguay',
        ]);

        DB::table('countries')->insert([
            'code' => 'PE',
            'name' => 'Perú',
        ]);

        DB::table('countries')->insert([
            'code' => 'PN',
            'name' => 'Islas Pitcairn',
        ]);

        DB::table('countries')->insert([
            'code' => 'PF',
            'name' => 'Polinesia Francesa',
        ]);

        DB::table('countries')->insert([
            'code' => 'PL',
            'name' => 'Polonia',
        ]);

        DB::table('countries')->insert([
            'code' => 'PT',
            'name' => 'Portugal',
        ]);

        DB::table('countries')->insert([
            'code' => 'PR',
            'name' => 'Puerto Rico',
        ]);

        DB::table('countries')->insert([
            'code' => 'QA',
            'name' => 'Qatar',
        ]);

        DB::table('countries')->insert([
            'code' => 'GB',
            'name' => 'Reino Unido',
        ]);

        DB::table('countries')->insert([
            'code' => 'RE',
            'name' => 'Reunión',
        ]);

        DB::table('countries')->insert([
            'code' => 'RW',
            'name' => 'Ruanda',
        ]);

        DB::table('countries')->insert([
            'code' => 'RO',
            'name' => 'Rumania',
        ]);

        DB::table('countries')->insert([
            'code' => 'RU',
            'name' => 'Rusia',
        ]);

        DB::table('countries')->insert([
            'code' => 'EH',
            'name' => 'Sahara Occidental',
        ]);

        DB::table('countries')->insert([
            'code' => 'SB',
            'name' => 'Islas Salomón',
        ]);

        DB::table('countries')->insert([
            'code' => 'WS',
            'name' => 'Samoa',
        ]);

        DB::table('countries')->insert([
            'code' => 'AS',
            'name' => 'Samoa Americana',
        ]);

        DB::table('countries')->insert([
            'code' => 'KN',
            'name' => 'San Cristóbal y Nevis',
        ]);

        DB::table('countries')->insert([
            'code' => 'SM',
            'name' => 'San Marino',
        ]);

        DB::table('countries')->insert([
            'code' => 'PM',
            'name' => 'San Pedro y Miquelón',
        ]);

        DB::table('countries')->insert([
            'code' => 'VC',
            'name' => 'San Vicente y las Granadinas',
        ]);

        DB::table('countries')->insert([
            'code' => 'SH',
            'name' => 'Santa Helena',
        ]);

        DB::table('countries')->insert([
            'code' => 'LC',
            'name' => 'Santa Lucía',
        ]);

        DB::table('countries')->insert([
            'code' => 'ST',
            'name' => 'Santo Tomé y Príncipe',
        ]);

        DB::table('countries')->insert([
            'code' => 'SN',
            'name' => 'Senegal',
        ]);

        DB::table('countries')->insert([
            'code' => 'CS',
            'name' => 'Serbia y Montenegro',
        ]);

        DB::table('countries')->insert([
            'code' => 'SC',
            'name' => 'Seychelles',
        ]);

        DB::table('countries')->insert([
            'code' => 'SL',
            'name' => 'Sierra Leona',
        ]);

        DB::table('countries')->insert([
            'code' => 'SG',
            'name' => 'Singapur',
        ]);

        DB::table('countries')->insert([
            'code' => 'SY',
            'name' => 'Siria',
        ]);

        DB::table('countries')->insert([
            'code' => 'SO',
            'name' => 'Somalia',
        ]);

        DB::table('countries')->insert([
            'code' => 'LK',
            'name' => 'Sri Lanka',
        ]);

        DB::table('countries')->insert([
            'code' => 'SZ',
            'name' => 'Suazilandia',
        ]);

        DB::table('countries')->insert([
            'code' => 'ZA',
            'name' => 'Sudáfrica',
        ]);

        DB::table('countries')->insert([
            'code' => 'SD',
            'name' => 'Sudán',
        ]);

        DB::table('countries')->insert([
            'code' => 'SE',
            'name' => 'Suecia',
        ]);

        DB::table('countries')->insert([
            'code' => 'CH',
            'name' => 'Suiza',
        ]);

        DB::table('countries')->insert([
            'code' => 'SR',
            'name' => 'Surinam',
        ]);

        DB::table('countries')->insert([
            'code' => 'SJ',
            'name' => 'Svalbard y Jan Mayen',
        ]);

        DB::table('countries')->insert([
            'code' => 'TH',
            'name' => 'Tailandia',
        ]);

        DB::table('countries')->insert([
            'code' => 'TW',
            'name' => 'Taiwán',
        ]);

        DB::table('countries')->insert([
            'code' => 'TZ',
            'name' => 'Tanzania',
        ]);

        DB::table('countries')->insert([
            'code' => 'TJ',
            'name' => 'Tayikistán',
        ]);

        DB::table('countries')->insert([
            'code' => 'IO',
            'name' => 'Territorio Británico del Océano Índico',
        ]);

        DB::table('countries')->insert([
            'code' => 'TF',
            'name' => 'Territorios Australes Franceses',
        ]);

        DB::table('countries')->insert([
            'code' => 'TL',
            'name' => 'Timor Oriental',
        ]);

        DB::table('countries')->insert([
            'code' => 'TG',
            'name' => 'Togo',
        ]);

        DB::table('countries')->insert([
            'code' => 'TK',
            'name' => 'Tokelau',
        ]);

        DB::table('countries')->insert([
            'code' => 'TO',
            'name' => 'Tonga',
        ]);

        DB::table('countries')->insert([
            'code' => 'TT',
            'name' => 'Trinidad y Tobago',
        ]);

        DB::table('countries')->insert([
            'code' => 'TN',
            'name' => 'Túnez',
        ]);

        DB::table('countries')->insert([
            'code' => 'TC',
            'name' => 'Islas Turcas y Caicos',
        ]);

        DB::table('countries')->insert([
            'code' => 'TM',
            'name' => 'Turkmenistán',
        ]);

        DB::table('countries')->insert([
            'code' => 'TR',
            'name' => 'Turquía',
        ]);

        DB::table('countries')->insert([
            'code' => 'TV',
            'name' => 'Tuvalu',
        ]);

        DB::table('countries')->insert([
            'code' => 'UA',
            'name' => 'Ucrania',
        ]);

        DB::table('countries')->insert([
            'code' => 'UG',
            'name' => 'Uganda',
        ]);

        DB::table('countries')->insert([
            'code' => 'UY',
            'name' => 'Uruguay',
        ]);

        DB::table('countries')->insert([
            'code' => 'UZ',
            'name' => 'Uzbekistán',
        ]);

        DB::table('countries')->insert([
            'code' => 'VU',
            'name' => 'Vanuatu',
        ]);

        DB::table('countries')->insert([
            'code' => 'VE',
            'name' => 'Venezuela',
        ]);

        DB::table('countries')->insert([
            'code' => 'VN',
            'name' => 'Vietnam',
        ]);

        DB::table('countries')->insert([
            'code' => 'VG',
            'name' => 'Islas Vírgenes Británicas',
        ]);

        DB::table('countries')->insert([
            'code' => 'VI',
            'name' => 'Islas Vírgenes de los Estados Unidos',
        ]);

        DB::table('countries')->insert([
            'code' => 'WF',
            'name' => 'Wallis y Futuna',
        ]);

        DB::table('countries')->insert([
            'code' => 'YE',
            'name' => 'Yemen',
        ]);

        DB::table('countries')->insert([
            'code' => 'DJ',
            'name' => 'Yibuti',
        ]);

        DB::table('countries')->insert([
            'code' => 'ZM',
            'name' => 'Zambia',
        ]);

        DB::table('countries')->insert([
            'code' => 'ZW',
            'name' => 'Zimbabue',
        ]);
    }
}
