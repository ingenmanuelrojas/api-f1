<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Pilot;
use Database\Factories\PilotFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->truncateTablas([
            'categories',
            'helps',
            'countries',
        ]);

        $this->call([
            CategorySeeder::class,
            HelpSeeder::class,
            CountrySeeder::class,
        ]);

        Pilot::factory(15)->create();
    }

    protected function truncateTablas(array $tablas)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
